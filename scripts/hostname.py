import os
import xml.etree.ElementTree as ET

# Path to the directory containing configuration files
CONFIG_DIR = "./configs/configs"

# Function to check if a file contains the phrase 'hostname'
def check_hostname_in_file(file_path):
    try:
        with open(file_path, 'r') as file:
            for line in file:
                if 'hostname' in line:
                    return True
    except Exception as e:
        print(f"Error reading {file_path}: {e}")
    return False

# Generate JUnit XML report for the requirement tests
def generate_junit_report(results):
    testsuite = ET.Element('testsuite', name="RequirementTests", tests=str(len(results)), failures=str(len([r for r in results if not r['status']])))
    
    for result in results:
        testcase = ET.SubElement(testsuite, 'testcase', classname="RequirementTests", name=result['name'])
        if not result['status']:
            failure = ET.SubElement(testcase, 'failure')
            failure.text = f"{result['name']} did not contain 'hostname'"
    
    # Create an XML tree and write to file
    tree = ET.ElementTree(testsuite)
    with open('test-reports/requirements_report.xml', 'wb') as f:
        tree.write(f)

# Main function to iterate over config files and check for 'hostname'
def main():
    if not os.path.exists('test-reports'):
        os.makedirs('test-reports')
    
    results = []
    
    # Iterate through all files in the config directory
    for root, dirs, files in os.walk(CONFIG_DIR):
        for file in files:
            file_path = os.path.join(root, file)
            print(f"Checking file: {file_path}")
            has_hostname = check_hostname_in_file(file_path)
            results.append({
                'name': f"Requirement: {file} contains 'hostname'",
                'status': has_hostname
            })
    
    # Generate the JUnit XML report
    generate_junit_report(results)
    print("JUnit report generated: test-reports/requirements_report.xml")

if __name__ == "__main__":
    main()
