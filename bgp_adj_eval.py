import os
import json
import pandas as pd
from pybatfish.client.session import Session
from pybatfish.datamodel import *
from pybatfish.datamodel.answer import *
from pybatfish.datamodel.flow import *

batfish_host = os.environ.get('BATFISH_HOST', 'localhost')
bf = Session(host=batfish_host)
NETWORK_NAME = "topology-eval"  # Name of the Batfish network
SNAPSHOT_PATH = "./configs"  # Path to network snapshot (configurations)
SNAPSHOT_NAME = "snapshot"

bf.set_network(NETWORK_NAME)
bf.init_snapshot(SNAPSHOT_PATH, name=SNAPSHOT_NAME, overwrite=True)

parse_status = bf.q.fileParseStatus().answer().frame()
print("Parsed Files:\n", parse_status)

bgpSessionCompatibility = bf.q.bgpSessionCompatibility().answer().frame()
print("BGP Session Compatibility:\n", bgpSessionCompatibility.head(3))

bgpSessionStatus = bf.q.bgpSessionStatus().answer().frame()
print("BGP Session Status:\n", bgpSessionStatus.head(3))

#with open("bgp_session_summary.txt", "w") as summary_file:
#    for index, row in bgpSessionStatus.iterrows():
#        summary_file.write(f"Status: {row['Established_Status']}\n")

established_sessions = bgpSessionStatus[bgpSessionStatus['Established_Status'] == 'ESTABLISHED']
num_established_peers = len(established_sessions)

metrics = {
            "name": "BGP Sessions Established",
            "value": num_established_peers
            }

# Save the metrics to a YAML file that GitLab can parse in the Merge Request widget
with open("bgp_session_summary.txt", "w") as metrics_file:
    json.dump(metrics, metrics_file, indent=2)

# glpat-LhF9AgqmutXB3K-uJfg6