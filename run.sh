#!/bin/sh
if docker ps | wc -l > 1
then
  sudo clab deploy --topo clab.yml --reconfigure
else
  sudo clab deploy --topo clab.yml
fi

