# GitOps for Network Engineers

## Project Purpose:

This project is used to house a demonstrative use-case for employing a Git focused approach for managing network devices inclusive of linting, testing, ephemeral labbing, and deployment. 

## Topology:

As of now, due to this being a demonstration the network topology itself is less important. As such, the network topology is simply two FRR routers with a BGP adjacency 

## Workflow: 

From the perspective of a Network Engineer:

### Planning
- Workring from Epics/Issues to dictate work to be done
### Working
- Opening a Merge Request, a separate branch, to being actual implementation of changes discussed in Epics/Issues
### Iterative Testing
- Rely on GitLab CI to iterate through commong testing based on changes made

1. TODO - Add pipeline schematic.
* Current example shown here: https://gitlab.com/gl-demo-ultimate-bdowning/netdevops/corp-dc/-/merge_requests/10

### Deployment
- Invoke deployment of approved code via merging into main and triggering deployment job via Git Tag (todo)

## Thoughts:



